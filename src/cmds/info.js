const Discord = require('discord.js');
const data = require('../data');

/**
 * @type {string}
 */
exports.lists_text = "`$info` Send information of Hoder";

/**
 * Run function
 * @param {string[]} params 
 * @param {string} id
 */
exports.run = (params, id) => {
    /** @type {Discord.TextChannel} */
    let channel = data.get_data(id, 'channel');

    let message = new Discord.MessageEmbed({
        color: 0x0099ff,
        title: "Hoder Information",
        description: "Hoder is a multi-function bot, you can add listener associate to a message and add emoji associate with a role",
        thumbnail: {
            url: "https://cdn.discordapp.com/avatars/784064700681945158/101864f6d22d4e0cb9a776114a4dcaac.png?size=1024"
        },
        author: {
            name: "Dragnansia",
            iconURL: "https://cdn.discordapp.com/avatars/358303310597324800/1e454b5580a23cb0bae5b687842a1248.png?size=128"
        },
        fields: [
            { name: "View All Command", value: "`$command` or `$help`"},
            { name: "Command Help", value: "`$(all command) help`" },
            { name: "Command info", value: "`{param}` is a required parameter\n`[param]` is a optional parameter" },
        ],
        footer: {
            iconURL: "https://cdn.discordapp.com/avatars/784064700681945158/101864f6d22d4e0cb9a776114a4dcaac.png?size=128",
            text: "All right reserve"
        }
    });

    channel.send(message);

    data.delete(id);
};
const Discord = require('discord.js');
const fs = require('fs');

/** Ajout de toutes les commandes */
let commands_bot = {};

/**
 * Use to include all command at index of the file name
 */
fs.readdir("./src/cmds", (err, files) => {
    files.forEach(f => {
        let name = f.split('.')[0];
        commands_bot[name] = require("./cmds/" + name);
    });
});

/**
 * Permet de lancer une commande seulement si celle-ci est trouvée
 * @param {string} command 
 * @param {string[]} params
 * @param {Discord.TextChannel} channel 
 * @param {Discord.User} author 
 * @param {CallableFunction} cb 
 */
exports.start_command = (command, params, id) => {
    if (command === "help")
        commands_bot["command"].run(params, id);
    else if (Object.keys(commands_bot).indexOf(command) !== -1)
        commands_bot[command].run(params, id);
};

/**
 * return all command
 */
exports.get_commands = () => {
    return commands_bot;
}
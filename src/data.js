let data = {};

/**
 * Add empty data and generate id
 * return true if insertion is good
 * @param {string} id
 * @param {*} d
 */
exports.add = (id, d = {}) => {
	if (id_exist(id)) {
        console.log("Donnée déjà existante");
        return false;
    }
    
    data[id] = d;
    return true;
}

/**
 * set data id
 * @param {string} id 
 * @param {*} d 
 */
exports.set = (id, d) => {
    if (!id_exist(id)) {
        console.log("N'a pas de donnée avec cette id");
        return;
    }

    data[id] = d;
}

/**
 * return data with id and name
 * @param {string} id 
 * @param {string} name 
 */
exports.get = (id) => {
    if (!id_exist(id)) {
        console.log("N'a pas de donnée avec cette id");
        return undefined;
    }

    return data[id];
}

/**
 * return data with id and name
 * @param {string} id 
 * @param {string} name 
 */
exports.get_data = (id, name) => {
    if (!id_exist(id)) {
        console.log("N'a pas de donnée avec cette id");
        return undefined;
    }

    return data[id][name];
}

/**
 * Change data of id and name
 * @param {string} id 
 * @param {string} name 
 * @param {*} d 
 */
exports.set_data = (id, name, d) => {
    if (!id_exist(id)) {
        console.log("N'a pas de donnée avec cette id");
        return;
    }

    data[id][name] = d;
}

/**
 * delete data
 * @param {string} id 
 */
exports.delete = (id) => {
    if (!id_exist(id)) {
        console.log("n'a pas de donnée avec cette id");
        return;
    }

    delete data[id];
}

/**
 * return true if is found a id
 * @param {string} id 
 */
function id_exist(id) {
    return data[id] !== undefined;
}
